class timeDate {
  constructor() {
    this.now = new Date();
  }

  year() {
    return this.now.getFullYear();
  }
}

export default timeDate;


