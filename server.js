const {Nuxt, Builder} = require('nuxt');
const bodyParser = require('body-parser');
const session = require('express-session');
const app = require('express')();

app.use(bodyParser.json());


const isProd = process.env.NODE_ENV === 'production';
const nuxt = new Nuxt({dev: !isProd});


if (!isProd) {
    const builder = new Builder(nuxt);
    builder.build();
}
app.use(nuxt.render);

app.post('/login', function (req, res) {
    console.log(req.body, res);
});

app.listen(3000);
console.log('Server is listening on http://localhost:3000');